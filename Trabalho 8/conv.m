function res = conv(a, b)
    % a help function to perform convolution on matrix operators
    % and cells
    % this function causes the most performance hit for EHD
    % testing showed that the below direct calculations, are 50% faster than 
    % abs(conv2(a, b, 'valid')) or even abs(sum(a .* b, 'all'))
    res = abs(a(1, 1) * b(1, 1) + a(2, 1) * b(2, 1) + a(2, 2) * b(2, 2) + a(1, 2) * b(1, 2));
end