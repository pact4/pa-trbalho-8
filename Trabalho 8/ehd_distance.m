function dist=ehd_distance(v1, v2)

local_bins1 = v1(1:80);
local_bins2 = v2(1:80);

global_bins1 = v1(81:85);
global_bins2 = v2(81:85);

dist = sum(abs(local_bins1 - local_bins2)) + 5 * sum(abs(global_bins1 - global_bins2));