function bins=computeEHD(subimg, T)

vertical_filter       = [1 -1; 1 -1];
horizontal_filter     = [1 1; -1 -1];
degrees45_filter      = [sqrt(2) 0; 0 -sqrt(2)];
degrees135_filter     = [0 sqrt(2); sqrt(2) 0];
nondirectional_filter = [2 -2; -2 2];

bins = [0, 0, 0, 0, 0];

[r,c]=size(subimg(:,:,1)); % Altura x cumprimento
r=ceil(r/2); c=ceil(c/2);
r2=2*r; c2=2*c;

subimg=imresize(subimg,[r2, c2]);

total_blocks = 1;

for i = 1:r
    for j = 1:c
        block = subimg(2*(i-1)+1:2*i, 2*(j-1)+1:2*j, :);
        
        % compute EHD for sub-image and store in ehd matrix
        pv = conv(block, vertical_filter);
        ph = conv(block, horizontal_filter);
        pd45 = conv(block, degrees45_filter);
        pd135 = conv(block, degrees135_filter);
        pno = conv(block, nondirectional_filter);

        [value, index] = max([pv, ph, pd45, pd135, pno]);
        
        if value > T
            bins(index) = bins(index)+1;
        end
        
        total_blocks = total_blocks + 1;
    end
end 

bins = bins/total_blocks;
