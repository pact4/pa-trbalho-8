function quantized_ehd=quantization(ehd_vector)

% Nonlinear quantization
num_bits = 3; % Number of bits for each bin
num_bins = length(ehd_vector);
q_levels = 2^num_bits - 1; % Number of quantization levels
max_val = max(ehd_vector);
quant_table = linspace(0, max_val, q_levels); % Quantization table

quantized_ehd = zeros(1, num_bins); % Initialize quantized EHD vector
for i = 1:num_bins
    bin_val = ehd_vector(i);
    [~, idx] = min(abs(quant_table - bin_val)); % Find closest quantization level
    quantized_ehd(i) = idx - 1; % Subtract 1 to get values between 0 and 7 (3 bits)
end