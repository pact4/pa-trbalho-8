function dist=cld_distance(v1,v2)
dist = sqrt(sum(2*((cldY1-cldY2).^2))) + sqrt(sum(2*((cldCb1-cldCb2).^2))) + sqrt(sum(4*((cldCr1-cldCr2).^2)));