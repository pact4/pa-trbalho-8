function ehd_vector=findehd(img)
[r,c]=size(img(:,:,1)); % Altura x cumprimento
r=ceil(r/4); c=ceil(c/4);
r4=4*r; c4=4*c;
img=imresize(img,[r4, c4]);

grayimg = rgb2gray(img);

% Divide the resized image into 16 sub-images
ehd = zeros(16, 5); % initialize EHD matrix
idx = 1;
for i = 1:4
    for j = 1:4
        sub_img = grayimg(r*(i-1)+1:r*i, c*(j-1)+1:c*j, :);
        % compute EHD for sub-image and store in ehd matrix
        ehd(idx,:) = computeEHD(sub_img, 50);
        idx = idx + 1;
    end
end

global_bin = mean(ehd);
ehd_vector = reshape(ehd', 1, []); % reshape the array to 1x80
%ehd_vector(end+1) = global_bin;
ehd_vector = [ehd_vector, global_bin];
disp(global_bin);
