function cld=findcld(img)
[r,c]=size(img(:,:,1));
r=ceil(r/8); c=ceil(c/8);
r8=8*r; c8=8*c;
disp('Original = ');
disp(num2str(size(img)));
img=imresize(img,[r8, c8]);
disp('Resized = ')
disp(num2str(size(img)));

oneblock=ones(r,c);
mask=[1 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0];

avg0 = @(block_struct) oneblock.*mean(mean(block_struct.data));
imgavg = uint8(blockproc(img,[r c], avg0));
disp('Average = ');
disp(num2str(size(imgavg)));
imgavg8 = imresize(imgavg,[64 64], 'nearest');
disp('Average8 = ');
disp(num2str(size(imgavg8)));
YCbCr = double(rgb2ycbcr(imgavg8));
disp('YCbCr = ');
disp(num2str(size(YCbCr)));

dctop = @(block_struct) dct2(block_struct.data).*mask;
Ydct = blockproc(YCbCr(:,:,1),[8 8],dctop);
disp('Ydct = ');
disp(num2str(size(Ydct)));
Cbdct = blockproc(YCbCr(:,:,2),[8 8],dctop);
Crdct = blockproc(YCbCr(:,:,3),[8 8],dctop);

[~,~,Yfv]=find(Ydct); [~,~,Cbfv]=find(Cbdct); [~,~,Crfv]=find(Crdct);
cld1=[Yfv Cbfv Crfv]; [rcld, ccld]=size(cld1);
cld = reshape(cld1,[1, rcld*ccld]);