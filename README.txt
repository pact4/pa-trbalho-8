Aula Laboratorial 8
Descritores	visuais MPEG-7 - Aplicação MATLAB

Todos os ficheiros que precisará para correr a aplicação encontram-se no seguinte link:
gitlab.com/pact4/pa-trbalho-8/-/edit/main/

Pre requisitos:
1. Deve fazer o download do source code do repositório GitLab fornecido.
2. Todos os ficheiros devem estar como no download, i.e., na mesma diretoria, incluindo a pasta 'Image_Dataset'

Deve seguir os seguintes passos para utilizar a aplicação desenvolvida:

1. Manter todos os ficheiros transferidos na mesma diretoria no seu computador
2. Abrir o MATLAB e correr o abrir o ficheiro app.mlapp. Ao fazer este passo o MATLAB deverá abrir uma nova janela - App Desginer
3. Deve correr a aplicação carregado no botão com o simbolo "play" a verde que se encontrará acima da maquete da aplicação.
4. Ao completar todos os outros passos o MATLAB deverá abrir novamente uma nova janela. agora com a aplicação.
5. Deve selecionar a imagem de pesquisa que prentende
6. Deve selecionar o descritor que prente de que seja calculado
7. Deve selecionar o número de imagens que pretende que lhe sejam devolvidas
8. Deve pressionar o botão "Analyze" para a aplicação calcular as imagens mais semelhantes.

A aplicação pode demorar alguns segundos até aparecer o resultado final.

NOTA: As imagens que são devolvidas, assim como a imagem de query, apenas aparecem na aplicação quando todo o processo de calculo das N imagens mais semelhantes tiver concluido, o que pode demorar algum tempo como referido anteriormente.


